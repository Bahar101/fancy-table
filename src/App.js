import React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import Layout from './components/layout';

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Layout />
    </DndProvider>
  );
}

export default App;









// import React from 'react';
// import MyTable from './components/myTable';
// import './App.css';

// function App() {
//   return (
//     <MyTable />
//   );
// }

// export default App;
