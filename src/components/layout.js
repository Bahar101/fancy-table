import React, { useState, useCallback } from 'react';
import MyList from './myList';
import update from 'immutability-helper';

import MyTable from './myTable';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import styles from './layout.module.css';

export default function Layout() {
    const [keyword, setKeyword] = useState('');

    const [data, setData] = useState([
        { id: 1, firstName: 'Reza', lastName: 'Hassani', address: 'Tehran', age: 24, education: '' },
        { id: 2, firstName: 'Bahar', lastName: 'Rezayee', address: 'Rasht', age: 21, education: '' },
    ])

    const [structure, setStructure] = useState([
        { id: 1, text: 'First Name', checked: true, dataIndex: 'firstName' },
        { id: 2, text: 'Last Name', checked: true, dataIndex: 'lastName' },
        { id: 3, text: 'Address', checked: false, dataIndex: 'address' },
        { id: 4, text: 'Age', checked: false, dataIndex: 'age' },
        { id: 5, text: 'Education', checked: false, dataIndex: 'education' }
    ]);

    const moveCard = useCallback(
        (dragIndex, hoverIndex) => {
            const dragCard = structure[dragIndex]
            setStructure(
                update(structure, {
                    $splice: [
                        [dragIndex, 1],
                        [hoverIndex, 0, dragCard],
                    ],
                }),
            )
        },
        [structure],
    )

    function handleCheckItem(id) {
        const index = structure.findIndex(item => item.id === id);
        const item = structure[index];
        const list1 = structure.slice(0, index);
        const list2 = structure.slice(index + 1);
        let newCard = [];
        if (item.checked) {
            newCard = [...list1, { ...item, checked: false }, ...list2];
        } else {
            newCard = [...list1, ...list2, { ...item, checked: true }];
        }
        setStructure(newCard);
    }

    const filteredCards = structure.filter(item => item.text.toLowerCase().includes(keyword.toLowerCase()));

    return (
        <Container style={{ margin: '30px auto' }} >
            <Row>
                <Col md={8}>
                    <MyTable columns={structure} data={data} />
                </Col>
                <Col md={{ span: 3, offset: 1 }}>
                    <InputGroup size="sm" className="mb-3">
                        <FormControl
                            type='text'
                            className={styles['search-btn']}
                            style={{ padding: '0 35px' }}
                            placeholder='Search'
                            value={keyword}
                            onChange={e => setKeyword(e.target.value)}
                        />
                    </InputGroup>
                    <MyList cards={filteredCards} moveCard={moveCard} onCheck={handleCheckItem} />
                </Col>
            </Row>
        </Container>
    )
}