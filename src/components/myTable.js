import React from 'react';
import TbodyModule from './tbodyModule';
import TheadModule from './theadModule';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';

export default function MyTable({ columns, data }) {
    return (
        <Container>
            <Table striped bordered hover>
                <TheadModule columns={columns} />
                <TbodyModule columns={columns} data={data} />
            </Table>
        </Container>
    )
}
