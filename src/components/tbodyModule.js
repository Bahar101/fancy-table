import React from 'react';

export default function TbodyModule({ data, columns }) {
    return (
        <tbody>
            {data.map((item) => {
                return (
                    <tr key={item.id}>
                        {columns.map(column => {
                            if (column.checked) {
                                return <td key={column.id}>{item[column.dataIndex]}</td>
                            }
                        })}
                    </tr>
                )
            })}
        </tbody>
    )
}
