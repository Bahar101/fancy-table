import React from 'react';
import { Card } from './card';
import ListGroup from 'react-bootstrap/ListGroup';

export default function MyList({ cards, moveCard, onCheck }) {
    return (
        <ListGroup>
            {cards.map((card, index) => {
                return card.checked && <ListGroup.Item variant="secondary" key={card.id}>
                    <Card
                        index={index}
                        key={card.id}
                        id={card.id}
                        text={card.text}
                        checked={card.checked}
                        moveCard={moveCard}
                        onCheck={onCheck}
                    />
                </ListGroup.Item>
            })}
            {cards.map((card, index) => {
                return !card.checked && <ListGroup.Item variant="secondary" key={card.id}>
                    <Card
                        key={card.id}
                        index={index}
                        id={card.id}
                        text={card.text}
                        checked={card.checked}
                        moveCard={moveCard}
                        onCheck={onCheck}
                    />
                </ListGroup.Item>
            })}
        </ListGroup>
    )
}
