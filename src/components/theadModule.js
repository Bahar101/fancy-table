import React from 'react';

export default function TheadModule({ columns }) {
    return (
        <thead>
            <tr>
                {columns.map(item => {
                    return  item.checked ? <th key={item.id}>{item.text}</th> : undefined
                })}
            </tr>
        </thead>
    )
}
